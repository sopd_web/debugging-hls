# Debugging an HTTP Live Stream used by hls.js

This is a repository documenting the debugging process of an HLS.

## The Problem

We are steaming an HLS using hls.js as a client. The stream freezes regulary and doesn't recover. The rate of occurance seems to depend on the available bandwith.

Stream URL: http://swisstxt2-lh.akamaihd.net/i/grischa_1@144003/master.m3u8
Code producing the issue: src/issue.js

## Plan

* Isolating the problem
* Creating an isolated test-case
* Find Problems
* Solve these problems

## Isolating the Problem

### Does the problem only occur with our stream?

Yes.

We used http://lsv.swisstxt.ch/lsv/StudiocamSRF3_1200/playlist.m3u8 to verify this. That stream works without a problem.

### What differences are there between the SRF and our stream?

The most striking irregularity between requests made by hls.js for our stream and the ones made for the SRF stream, is that there are duplicate requests made to the media playlist-file for our stream. That seems to be a result of the target duration is equal to the duration of the media files.

