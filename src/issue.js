var safeStart = (function () {
  var hls

  return function () {
    hls = stream(hls)
    setTimeout(safeStart, 5*60*1000)
  }
})()

// anonymous function 

function stream(hls) {
  var stream = "http://swisstxt2-lh.akamaihd.net/i/grischa_1@144003/master.m3u8"
  if(hls) {
    hls.destroy()
    delete hls
  }
  if(Hls.isSupported()) {
      var video = document.getElementById('video');
      var hls = new Hls();
      // bind them together
      hls.attachMedia(video);
      // MEDIA_ATTACHED event is fired by hls object once MediaSource is ready
      hls.on(Hls.Events.MEDIA_ATTACHED, function() {
          hls.loadSource(stream);
          // setTimeout(video.pause.bind(video), 2000)
          // setTimeout(video.play.bind(video), 10000) 
      });
    // listen to errors
    hls.on(Hls.Events.ERROR,function(event,data) {

    var errorType = data.type;
    var errorDetails = data.details;
    var errorFatal = data.fatal;

      if(data.fatal) {
        switch(data.type) {
        case Hls.ErrorTypes.NETWORK_ERROR:
        // Try to recover network error
          console.log("fatal network error encountered, try to recover");
          hls.startLoad();
          break; 
        case Hls.ErrorTypes.MEDIA_ERROR:
          console.log("fatal media error encountered, try to recover");
          hls.recoverMediaError();
          break;
        default: 
        //cannot recover
          hls.destroy();
          break; 
        }
      }
    })
    return hls
  }
}

safeStart()

