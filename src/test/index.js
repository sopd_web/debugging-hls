/* global Hls */

var stream = 'http://swisstxt2-lh.akamaihd.net/i/grischa_1@144003/master.m3u8'
// var stream = 'http://lsv.swisstxt.ch/lsv/StudiocamSRF3_1200/playlist.m3u8'

if (Hls.isSupported()) {
  var hls = new Hls()
  // bind them together
  hls.attachMedia(document.getElementsByTagName('video')[0])
  // MEDIA_ATTACHED event is fired by hls object once MediaSource is ready
  hls.on(Hls.Events.MEDIA_ATTACHED, function () {
    hls.loadSource(stream)
  })
  // listen to errors
  hls.on(Hls.Events.ERROR, function (event, data) {
    if (data.fatal) {
      switch (data.type) {
        case Hls.ErrorTypes.NETWORK_ERROR:
        // Try to recover network error
          console.log('fatal network error encountered, try to recover')
          hls.startLoad()
          break
        case Hls.ErrorTypes.MEDIA_ERROR:
          console.log('fatal media error encountered, try to recover')
          hls.recoverMediaError()
          break
        default:
        // cannot recover
          hls.destroy()
          break
      }
    }
  })
}

